pragma solidity ^0.7.6;

contract MultiSigWallet {

    event ActionConfirmed(bytes32 indexed id, address indexed sender);
    event RequestToAddOwner(address indexed newowner);
    event OwnerAdded(address indexed newowner);
    event RequestToRemoveOwner(address indexed owner);
    event OwnerRemoved(address indexed owner);
    event RequestToChangeThreshold(uint256 amount, uint256 oldthresh, uint256 newthresh);
    event ThresholdChanged(uint256 amount, uint256 oldthresh, uint256 newthresh);
    event RequestForTransfer(address indexed token, address indexed receiver, uint256 value);
    event TransferExecuted(address indexed token, address indexed receiver, uint256 value);
    event CancelRegistered(bytes32 indexed id, address indexed sender);
    event ActionCanceled(bytes32 indexed id);

    constructor (address[] memory _owners, uint _threshold) public {
        /* При развертывании контракта должен быть указан список аккаунтов-владельцев, 
           которые могут взаимодействовать с кошельком и пороговое значение, сколько
           из аккаунтов должно подтвердить то или иное действие.
        */
    }

    function addOwner(address _newowner) external {
        /* метод `addOwner`позволяет существующим владельцам кошелька добавлять нового
           владельца.
        */
    }

    function removeOwner(address _owner) external {
        /* метод `removeOwner`позволяет  владельцам кошелька удалить одного из
           существующих владельцев.
        */
    }

    function changeThreshold(uint256 _thresh) external {
        /* метод `changeThreshold`позволяет владельцам кошелька изменить пороговое
           значение необходимых подвтерждения.
        */
    }

    function transfer(address _receiver, uint256 _value) external {
        /* метод `transfer` позволяет владельцам кошелька отправить заданное количество
           ether, которые хранятся на балансе контракта-кошелька, на счет другого
           пользователя или контракта.
        */
    }

    function transfer(address _token, address _receiver, uint256 _value) external {
        /* метод `transfer` позволяет владельцам кошелька отправить заданное количество
           ERC20 токенов, определенных по адресу контракта, на счет другого пользователя
           или контракта.
        */
    }

    function cancel(bytes32 _id) external {
        /* метод `cancel` позволяет тем владельцам, которые уже подтвердили какое-то
           действие в кошельке, послать отмену своего подтверждения, указав уникальный
           идентификатор, присваиваемый действию в момент появления первого подвтерждения.
        */
    }
}
    
